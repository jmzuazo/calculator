'use strict';

/**
 * @ngdoc directive
 * @name calculatorApp.directive:calculatorKeyboard
 * @description
 * # calculatorKeyboard
 */
angular.module('calculatorApp')
  .directive('calculatorKeyboard', function () {
    return {
      templateUrl: 'views/calculator-keyboard.tpl.html',
      restrict: 'E',
      replace: true
    };
  });
