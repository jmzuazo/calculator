'use strict';

/**
 * @ngdoc directive
 * @name calculatorApp.directive:calculatorDisplay
 * @description
 * # calculatorDisplay
 */
angular.module('calculatorApp')
  .directive('calculatorDisplay', function () {
    return {
      templateUrl: 'views/calculator-display.tpl.html',
      restrict: 'E',
      replace: true
    };
  });
