'use strict';

/**
 * @ngdoc directive
 * @name calculatorApp.directive:calculatorHistory
 * @description
 * # calculatorHistory
 */
angular.module('calculatorApp')
  .directive('calculatorHistory', function () {
    return {
      templateUrl: 'views/calculator-history.tpl.html',
      restrict: 'E',
      replace: true,
      scope: { isHistoryVisible: '=isHistoryVisible', clearHistory: '=clearHistory', operationsRegistry: '=operationsRegistry' },
      link: function postLink(scope, element, attrs) {
        scope.title = "Operations History";

        scope.clearList = function() {
          scope.clearHistory();
        }
      }
    };
  });
