'use strict';

/**
 * @ngdoc service
 * @name calculatorApp.calculatorService
 * @description
 * # calculatorService
 * Service in the calculatorApp.
 */
angular.module('calculatorApp')
  .service('calculatorService', function ($http) {
    return {
      evaluateExpression: function(expression) {
        return $http.post('/calculator/evaluate', { 'expression': expression }).then(function(response) {
          return response.data;
        }, function(response) {
          return response.data;
        });
      },
      saveSession: function(operationsRegistry) {
        return $http.post('/calculator/session', { 'operationsRegistry': operationsRegistry }).then(function(response) {
          return response.data.operationsRegistry;
        });
      },
      restoreSession: function() {
        return $http.get('/calculator/session').then(function(response) {
          return response.data.operationsRegistry;
        });
      }
    };
  });
