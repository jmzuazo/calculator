'use strict';

/**
 * @ngdoc function
 * @name calculatorApp.controller:CalculatorCtrl
 * @description
 * # CalculatorCtrl
 * Controller of the calculatorApp
 */
angular.module('calculatorApp')
  .controller('CalculatorCtrl', function ($scope, calculatorService) {
    $scope.isHistoryVisible = false;
    $scope.input = '';
    $scope.operationsRegistry = [];

    $scope.toggleHistoryVisibility = function() {
      $scope.isHistoryVisible = !$scope.isHistoryVisible;
    };
    $scope.clearInput = function() {
      $scope.input = '';
    };
    $scope.clearHistory = function() {
      $scope.operationsRegistry = [];
    };
    $scope.updateInput = function($event) {
      var messagesList = ['Error', 'Session saved', 'Session recovered'];
      if (messagesList.indexOf($scope.input) > -1) {
        $scope.clearInput();
      }
      $scope.input += $event.target.value;
    };
    $scope.evaluateExpression = function() {
      calculatorService.evaluateExpression($scope.input).then(function(data) {
        $scope.input = data.result;
        if (typeof(data.operation) != 'undefined') {
          $scope.operationsRegistry.push(data.operation);
        }
      });
    };
    $scope.saveSession = function() {
      calculatorService.saveSession($scope.operationsRegistry).then(function(data) {
        $scope.input = 'Session saved';
      });
    };
    $scope.restoreSession = function() {
      calculatorService.restoreSession().then(function(data) {
        $scope.input = 'Session recovered';
        $scope.operationsRegistry = data;
      });
    };
  });
