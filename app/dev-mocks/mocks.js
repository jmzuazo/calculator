'use strict';

(function(isNode, isAngular) {

  var mockModules = [
    'ngMockE2E',
    'myAppMocks.calculatorServiceMock'
  ];

  function addAngularMockModule() {
    angular.module('myAppMocks', mockModules)
      .run(function($http, $httpBackend) {
        // Pass through all request
        $httpBackend.whenGET(/.*/).passThrough();
        $httpBackend.whenPOST(/.*/).passThrough();
      });

    //add myAppMocks as dependency of the app's main
    // module so it gets loaded
    angular.module('calculatorApp')
      .requires.push('myAppMocks');
  }

  if (isAngular) {
    addAngularMockModule();
  } else if (isNode) {
    //TODO: configure hooks.js for support of mocks on E2E test
    module.exports.MODULE_LIST = mockModules;
  } else {
    throw new Error('Error injecting mock module');
  }

})(typeof module !== 'undefined' && module.exports,
  typeof angular !== 'undefined');
