'use strict';

angular.module('myAppMocks.calculatorServiceMock', [])
  .run(function($httpBackend) {
    var operationsRegistry = [];

    // evaluate current expression
    $httpBackend.whenPOST('/calculator/evaluate').respond(function(method, url, data) {
      var data = JSON.parse(data),
          result = '';

      try {
        result = eval(data.expression.replace(/log([0-9.]+)/g, 'Math.log($1)'));
      } catch(e) {
        return [400, { 'result': 'Error' }];
      }

      return [200, {
        'operation': data.expression + '=' + result,
        'result': result
      }];
    });

    // returns the last session
    $httpBackend.whenGET('/calculator/session').respond(function(method, url, data) {
      return [200, { 'operationsRegistry': operationsRegistry }];
    });

    // save the current session
    $httpBackend.whenPOST('/calculator/session').respond(function(method, url, data) {
      var data = JSON.parse(data);

      operationsRegistry = data.operationsRegistry;
      return [200, { 'operationsRegistry': operationsRegistry }];
    });
  });
