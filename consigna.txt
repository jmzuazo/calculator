Ejercicio
Objetivo: Desarrollar el frontend web para una calculadora.

La calculadora cuenta con las siguientes funcionalidades:

la calculadora soporta:
suma,
resta,
multiplicación,
división,
logaritmo con números de punto flotante.
Ejemplo: (2+2)*log 10/3
se permite persistir una sesion de cálculo
se permite recuperar una sesión:
Ejemplo:
input: 2+2
output: 4
input: 5*3*(8-23)
output: -225
input: guardar sesion1
output: sesion1 almacenada
input: recuperar sesion1
output: 2+2
= 4
5*3*(8-23)
     = -225

Asumpciones y limitantes:

Se asume que la lógica de la calculadora y de sesión se provee a través de una API REST sobre JSON.
Esto debe ser mockeado o simulado de alguna manera simple. Es suficiente con respuestas hardcodeadas.
Un objetivo del ejercicio es ver que el front-end esté desacoplado completamente del backend.
Por más que el backend sea simulado, se tiene que poder ver esta característica en el código.
El aspecto visual del frontend importa.
Se tiene que usar Javascript para el desarrollo. Aparte de eso no hay ningún limitante en cuanto librerías o frameworks a utilizar.
