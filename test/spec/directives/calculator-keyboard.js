'use strict';

describe('Directive: calculatorKeyboard', function () {

  // load the directive's module
  beforeEach(module('calculatorApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<calculator-keyboard></calculator-keyboard>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the calculatorKeyboard directive');
  }));
});
