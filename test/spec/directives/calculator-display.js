'use strict';

describe('Directive: calculatorDisplay', function () {

  // load the directive's module
  beforeEach(module('calculatorApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<calculator-display></calculator-display>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the calculatorDisplay directive');
  }));
});
